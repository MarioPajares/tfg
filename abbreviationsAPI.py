import urllib
from xml.etree import ElementTree as ET


def get_abbreviations(word):
    print("Ejecutado de API")
    requestURL = "https://www.abbreviations.com/services/v2/abbr.php?uid=6346&tokenid=QsJwltBgC9YVE7u2&term="+str(word)
    request = urllib.urlopen(requestURL)
    root = ET.parse(request).getroot()

    items = root.findall('result')
    if items != []:
        return items[0].find('definition').text
    else:
        return "None"
