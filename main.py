#!/usr/local/bin/python
# -*- encoding: utf-8 -*-
import difflib
import sys
import jellyfish
import os
from Levenshtein import distance
from nltk.corpus import wordnet as wn
import generateRDF
import lov
import normalizarResultados as normalizarResultados
import propiedades
import query
import salidajson
import prefix
import sqlite
import tokenizer


def calcular_sinonimos(propiedadDic1, diccionario2, array_para_salidas_dataset1, array_para_salidas_dataset2):
    propiedadAuxiliar = propiedades.Propiedad(" ", " ", " ", " ", " ", " ", " ")
    mejorResultadoDifflibSinonimo= [-1, propiedadAuxiliar, 0]
    mejorResultadoDistanciaLevenshteinSinonimo = [1000, propiedadAuxiliar, 0]
    mejorResultadodistanceDemerauSinonimo = [1000, propiedadAuxiliar, 0]
    mejorResultadodistanceHammingSinonimo = [1000, propiedadAuxiliar, 0]
    mejorResultadodistanceJaroSinonimo = [-1, propiedadAuxiliar, 0]
    mejorPonderadoDistanciasSinonimo = [0, propiedadAuxiliar]
    arrayTokenizadoPropiedad1 = tokenizer.doTokenizer(propiedadDic1.getName())
    for key_dic_2 in diccionario2:
        propiedadDic2 = diccionario2[key_dic_2][0]
        arrayTokenizadoPropiedad2 = tokenizer.doTokenizer(propiedadDic2.getName())
        for propiedadTokenizada1 in arrayTokenizadoPropiedad1:
            arraySinonimosPropiedad1 = wn.synsets(str(propiedadTokenizada1))
            for propiedadTokenizada2 in arrayTokenizadoPropiedad2:
                arraySinonimosPropiedad2 = wn.synsets(str(propiedadTokenizada2))
                for sinonimoPropiedad1 in arraySinonimosPropiedad1:
                    for sinonimoPropiedad2 in arraySinonimosPropiedad2:
                        aux1Word = getWordFromSynset(sinonimoPropiedad1)
                        aux2Word = getWordFromSynset(sinonimoPropiedad2)
                        ratioSin = difflib.SequenceMatcher(None, aux1Word, str(aux2Word)).ratio()
                        distanciaSin = distance(aux1Word, aux2Word)
                        distanceDemerauSin = jellyfish.damerau_levenshtein_distance(unicode(aux1Word), unicode(aux2Word))
                        distanceHammingSin = jellyfish.hamming_distance(unicode(aux1Word), unicode(aux2Word))
                        distanceJaroSin = jellyfish.jaro_distance(unicode(aux1Word), unicode(aux2Word))

                        valorPonderadoDistanciasSinonimo = ratioSin + float(normalizarResultados.normalizarSalida(distanciaSin, len(aux2Word))) + float(normalizarResultados.normalizarSalida(distanceDemerauSin, len(aux2Word))) + float(distanceJaroSin) + float(normalizarResultados.normalizarSalida(distanceHammingSin, len(aux2Word)))
                        valorPonderadoDistanciasSinonimo = calcularMediaPonderada(valorPonderadoDistanciasSinonimo, 5)
                        mejorPonderadoDistanciasSinonimo = actualizarMejorMediaPonderadaSinonimos(valorPonderadoDistanciasSinonimo, mejorPonderadoDistanciasSinonimo, propiedadDic2, sinonimoPropiedad1, sinonimoPropiedad2, propiedadDic1)

                        if ratioSin > mejorResultadoDifflibSinonimo[0]:
                            mejorResultadoDifflibSinonimo = [ratioSin, propiedadDic2]
                            # Calculo mejor alogritmo Levenshtein Distance
                        if distanciaSin < mejorResultadoDistanciaLevenshteinSinonimo[0]:
                            mejorResultadoDistanciaLevenshteinSinonimo = [distanciaSin, propiedadDic2, max(len(aux2Word), len(aux1Word))]
                            # Calculo mejor alogritmo distanceDemerau
                        if distanceDemerauSin < mejorResultadodistanceDemerauSinonimo[0]:
                            mejorResultadodistanceDemerauSinonimo = [distanceDemerauSin, propiedadDic2, max(len(aux2Word), len(aux1Word))]
                            # Calculo mejor alogritmo de HAMMING
                        if distanceHammingSin < mejorResultadodistanceHammingSinonimo[0]:
                            mejorResultadodistanceHammingSinonimo = [distanceHammingSin, propiedadDic2, max(len(aux2Word), len(aux1Word))]
                            # Calculo mejor alogritmo de Jaro
                        if distanceJaroSin > mejorResultadodistanceJaroSinonimo[0]:
                            mejorResultadodistanceJaroSinonimo = [distanceJaroSin, propiedadDic2]

    array_para_salidas_dataset1, array_para_salidas_dataset2 = comprobar_mejora_resultados(propiedadDic1, array_para_salidas_dataset1, array_para_salidas_dataset2, mejorPonderadoDistanciasSinonimo)
    return array_para_salidas_dataset1, array_para_salidas_dataset2


def comprobar_mejora_resultados(propiedad1, array_para_salidas_dataset1, array_para_salidas_dataset2, mejorPonderadoS):
    for propiedad1_aux in array_para_salidas_dataset1:
        if propiedad1_aux.getName() == propiedad1.getName():
            if propiedad1_aux.getCoincidencia()[0] < mejorPonderadoS[0]:
                # print("Se ha mejorado el valor de:", str(propiedad1.getName()), " con ", str(mejorPonderadoS[1].getName()))
                propiedad1_aux.setCoincidencia(mejorPonderadoS)
                array_para_salidas_dataset2 = actualizar_resultado_cambio(propiedad1_aux, array_para_salidas_dataset2, mejorPonderadoS[0])
    return array_para_salidas_dataset1, array_para_salidas_dataset2


def actualizar_resultado_cambio(propiedad1, array_para_salidas_dataset2, value):
    for propiedad2_aux in array_para_salidas_dataset2:
        if propiedad2_aux.getName() == propiedad1.getCoincidencia()[1].getName():
            if propiedad2_aux.getCoincidencia() == []:
                propiedad2_aux.setCoincidencia([value, propiedad1])

            elif propiedad2_aux.getCoincidencia()[0] < value:
                propiedad2_aux.setCoincidencia([value, propiedad1])

    return array_para_salidas_dataset2


def calcular_distancias_significado(propiedadDic1, diccionario2, array_para_salidas_dataset1, array_para_salidas_dataset2):
    propiedadAuxiliar = propiedades.Propiedad(" ", " ", " ", " ", " ", " ", " ")
    mejorNltkWup = [0, propiedadAuxiliar]
    mejorNltkpath = [0, propiedadAuxiliar]
    arrayTokenizadoPropiedad1 = tokenizer.doTokenizer(propiedadDic1.getName())
    for key_dic_2 in diccionario2:
        propiedadDic2 = diccionario2[key_dic_2][0]
        arrayTokenizadoPropiedad2 = tokenizer.doTokenizer(propiedadDic2.getName())
        for propiedadTokenizada1 in arrayTokenizadoPropiedad1:
            arraySinonimosPropiedad1 = wn.synsets(str(propiedadTokenizada1))
            for propiedadTokenizada2 in arrayTokenizadoPropiedad2:
                arraySinonimosPropiedad2 = wn.synsets(str(propiedadTokenizada2))
                for sinonimoPropiedad1 in arraySinonimosPropiedad1:
                    for sinonimoPropiedad2 in arraySinonimosPropiedad2:
                        # Calculo wup_similarity
                        if sinonimoPropiedad1.wup_similarity(sinonimoPropiedad2) > mejorNltkWup[0]:
                            var = sinonimoPropiedad1.wup_similarity(sinonimoPropiedad2)
                            mejorNltkWup = [sinonimoPropiedad1.wup_similarity(sinonimoPropiedad2), propiedadDic2]

                        # Calculo path_similarity
                        resultadopathvar = sinonimoPropiedad1.path_similarity(sinonimoPropiedad2)
                        if sinonimoPropiedad1.path_similarity(sinonimoPropiedad2) > mejorNltkpath[0]:
                            var2 = sinonimoPropiedad1.path_similarity(sinonimoPropiedad2)
                            mejorNltkpath = [sinonimoPropiedad1.path_similarity(sinonimoPropiedad2), propiedadDic2]
                            # valorPonderadoSignificado += float(sinonimoPropiedad1.path_similarity(sinonimoPropiedad2))
                        # mejorPonderadoSignificado = actualizarMejorMediaPonderada(calcularMediaPonderada(valorPonderadoSignificado, 2), mejorPonderadoSignificado, propiedadDic2)

    array_para_salidas_dataset1, array_para_salidas_dataset2 = comprobar_mejora_resultados(propiedadDic1, array_para_salidas_dataset1, array_para_salidas_dataset2, mejorNltkpath)
    return array_para_salidas_dataset1, array_para_salidas_dataset2


def calcular_distancias(propiedadDic1, diccionario2, array_para_salidas_dataset1, array_para_salidas_dataset2):
    global filaPonderado
    propiedadAuxiliar = propiedades.Propiedad(" ", " ", " ", " ", " ", " ", " ")
    # Resultados algoritmos normales
    mejorResultadoDifflib = [-1, ' ', 0]
    mejorResultadoDistanciaLevenshtein = [1000, ' ', 0]
    mejorResultadodistanceDemerau = [1000, ' ', 0]
    mejorResultadodistanceHamming = [1000, ' ', 0]
    mejorResultadodistanceJaro = [-1, ' ', 0]
    valorPonderadoDistancias = 0
    mejorPonderadoDistancias = [0, propiedadAuxiliar]
    es_exacto = False
    word_long1 = ""
    if len(propiedadDic1.getName()) < 5:
        word_long1 = sqlite.find_abbreviation(propiedadDic1.getName())
    for key_dic_2 in diccionario2:
        propiedadDic2 = diccionario2[key_dic_2][0]
        if propiedadDic2.getName() == "streetaddress":
            pass
        if propiedadDic2.getName() == propiedadDic1.getName():
            mejorResultadoDifflib = [1, propiedadDic2]
            mejorResultadoDistanciaLevenshtein = [1, propiedadDic2, len(propiedadDic2.getName()) ]
            mejorResultadodistanceDemerau = [1, propiedadDic2, len(propiedadDic2.getName()) ]
            mejorResultadodistanceHamming = [1, propiedadDic2, len(propiedadDic2.getName()) ]
            mejorResultadodistanceJaro = [1, propiedadDic2]
            mejorPonderadoDistancias[0] = 1
            mejorPonderadoDistancias[1] = propiedadDic2
            es_exacto = True
        elif word_long1 == propiedadDic2.getName():
            mejorResultadoDifflib = [1, propiedadDic2]
            mejorResultadoDistanciaLevenshtein = [1, propiedadDic2, len(propiedadDic2.getName()) ]
            mejorResultadodistanceDemerau = [1, propiedadDic2, len(propiedadDic2.getName()) ]
            mejorResultadodistanceHamming = [1, propiedadDic2, len(propiedadDic2.getName()) ]
            mejorResultadodistanceJaro = [1, propiedadDic2]
            mejorPonderadoDistancias[0] = 1
            mejorPonderadoDistancias[1] = propiedadDic2
            es_exacto = True
        elif len(propiedadDic2.getName()) < 5:
            word_long2 = sqlite.find_abbreviation(propiedadDic2.getName())
            if word_long2 == propiedadDic1.getName():
                mejorResultadoDifflib = [1, propiedadDic2]
                mejorResultadoDistanciaLevenshtein = [1, propiedadDic2, len(propiedadDic2.getName())]
                mejorResultadodistanceDemerau = [1, propiedadDic2, len(propiedadDic2.getName())]
                mejorResultadodistanceHamming = [1, propiedadDic2, len(propiedadDic2.getName())]
                mejorResultadodistanceJaro = [1, propiedadDic2]
                mejorPonderadoDistancias[0] = 1
                mejorPonderadoDistancias[1] = propiedadDic2
                es_exacto = True

        if es_exacto == False:
            if len(propiedadDic2.getName()) > 0:
                distanciaDifflib = difflib.SequenceMatcher(None, str(propiedadDic1.getName()), str(propiedadDic2.getName())).ratio()
                distanciaLevenshtein = distance(str(propiedadDic1.getName()), str(propiedadDic2.getName()))
                distanceDemerau = jellyfish.damerau_levenshtein_distance(unicode(propiedadDic1.getName()), unicode(propiedadDic2.getName()))
                distanceHamming = jellyfish.hamming_distance(unicode(propiedadDic1.getName()), unicode(unicode(propiedadDic2.getName())))
                distanceJaro = jellyfish.jaro_distance(unicode(propiedadDic1.getName()), unicode(propiedadDic2.getName()))

                # Calculo mejor alogritmo DIFFLIB
                if distanciaDifflib > mejorResultadoDifflib[0]:
                    mejorResultadoDifflib = [distanciaDifflib, propiedadDic2]
                valorPonderadoDistancias = distanciaDifflib

                # Calculo mejor alogritmo Levenshtein Distance
                if distanciaLevenshtein < mejorResultadoDistanciaLevenshtein[0]:
                    mejorResultadoDistanciaLevenshtein = [distanciaLevenshtein, propiedadDic2, max(len(propiedadDic1.getName()), len(propiedadDic2.getName()))]
                valorPonderadoDistancias += float(normalizarResultados.normalizarSalida(distanciaLevenshtein, max(len(propiedadDic1.getName()), len(propiedadDic2.getName()))))

                # Calculo mejor alogritmo distanceDemerau
                if distanceDemerau < mejorResultadodistanceDemerau[0]:
                    mejorResultadodistanceDemerau = [distanceDemerau, propiedadDic2, max(len(propiedadDic1.getName()),len(propiedadDic2.getName())) ]
                valorPonderadoDistancias += float(normalizarResultados.normalizarSalida(distanceDemerau, max(len(propiedadDic1.getName()),len(propiedadDic2.getName()))))

                # Calculo mejor alogritmo de HAMMING
                if distanceHamming < mejorResultadodistanceHamming[0]:
                    mejorResultadodistanceHamming = [distanceHamming, propiedadDic2, max(len(propiedadDic1.getName()), len(propiedadDic2.getName()))]
                valorPonderadoDistancias += float(normalizarResultados.normalizarSalida(distanceHamming, max(len(propiedadDic1.getName()), len(propiedadDic2.getName()))))

                # Calculo mejor alogritmo de Jaro
                if distanceJaro > mejorResultadodistanceJaro[0]:
                    mejorResultadodistanceJaro = [distanceJaro, propiedadDic2]
                valorPonderadoDistancias += float(distanceJaro)

                mejorPonderadoDistancias = actualizarMejorMediaPonderada(calcularMediaPonderada(valorPonderadoDistancias, 5), mejorPonderadoDistancias, propiedadDic2)
                # mejorPonderadoDistancias(array_para_salidas_dataset2, propiedadDic2, mejorPonderadoDistancias[0], mejorPonderadoDistancias[1])

    array_para_salidas_dataset1, array_para_salidas_dataset2 = actualizar_datos_array_para_salidas(array_para_salidas_dataset1, array_para_salidas_dataset2, propiedadDic1, mejorPonderadoDistancias[0], mejorPonderadoDistancias[1])

    return array_para_salidas_dataset1, array_para_salidas_dataset2, es_exacto


# Devuelve el mejor ponderado
def actualizarMejorMediaPonderada(val, mejorPonderado, propiedad):
    if val > mejorPonderado[0]:
        mejorPonderado[0] = val
        mejorPonderado[1] = propiedad
    return mejorPonderado


# Devuelve el mejor ponderado
def actualizarMejorMediaPonderadaSinonimos(val, mejorPonderado, propiedad, sinonimoPropiedad1, sinonimoPropiedad2, propiedadDic1):
    if val > mejorPonderado[0]:
        mejorPonderado[0] = val
        mejorPonderado[1] = propiedad
    return mejorPonderado


def actualizar_datos_array_para_salidas(array_para_salidas1, array_para_salidas2, propiedad, value, propiedad_coincidencia):
    for propiedad_array in array_para_salidas1:
        if propiedad.getName() == propiedad_array.getName():
            if propiedad_array.getCoincidencia() == []:
                    propiedad_array.setCoincidencia([value, propiedad_coincidencia])
                    array_para_salidas2 = actualizar_datos_segundo_array(array_para_salidas2, propiedad_coincidencia, value, propiedad)
            elif propiedad_array.getCoincidencia()[0] < value:
                propiedad_array.setCoincidencia([value, propiedad_coincidencia])
                array_para_salidas2 = actualizar_datos_segundo_array(array_para_salidas2, propiedad_coincidencia, value, propiedad)
    return array_para_salidas1, array_para_salidas2


def actualizar_datos_segundo_array(array_para_salidas2, propiedad_coincidencia, value, propiedad_array1):
    for propiedad_auxiliar in array_para_salidas2:
        if propiedad_auxiliar.getName() == propiedad_coincidencia.getName():
            if propiedad_auxiliar.getCoincidencia() == []:
                propiedad_auxiliar.setCoincidencia([value, propiedad_array1])
            elif propiedad_auxiliar.getCoincidencia()[0] < value:
                propiedad_auxiliar.setCoincidencia([value, propiedad_array1])
    return array_para_salidas2

def calcularMediaPonderada(val, numElemtos):
    return float(val / numElemtos)


def getWordFromSynset(word):
    return str(word).split("'")[1].split(".")[0]


def compareProps(diccionario1, diccionario2, ratio, nombreRDF):
    global fila
    errorLOV = False
    array_para_salidas_dataset1, array_para_salidas_dataset2 = add_propiedades_array_salida(diccionario1, diccionario2)
    for key_dic_1 in diccionario1:
        propiedadDic1 = diccionario1[key_dic_1][0]
        array_para_salidas_dataset1, array_para_salidas_dataset2, es_exacto = calcular_distancias(propiedadDic1, diccionario2, array_para_salidas_dataset1, array_para_salidas_dataset2)
        fila += 1
    fila = 1
    ponderado_tabla1 = []
    ponderado_tabla2 = []

    for prop in array_para_salidas_dataset1:
        ponderado_tabla1 = salidajson.add_distancia_semantica_json(prop, ponderado_tabla1)
    print len(ponderado_tabla1)
    for prop in array_para_salidas_dataset2:
        ponderado_tabla2 = salidajson.add_distancia_semantica_json(prop, ponderado_tabla2)
    print("Terminado Distancia sematantica")

    for key_dic_1 in diccionario1:
        propiedadDic1 = diccionario1[key_dic_1][0]
        if not coincidencia_exacta(propiedadDic1, array_para_salidas_dataset1):
            array_para_salidas_dataset1, array_para_salidas_dataset2 = calcular_sinonimos(propiedadDic1, diccionario2, array_para_salidas_dataset1, array_para_salidas_dataset2)
        fila += 1
    fila = 1

    for prop in array_para_salidas_dataset1:
        ponderado_tabla1 = salidajson.add_distancia_sinonimo_json(prop, ponderado_tabla1)
    for prop in array_para_salidas_dataset2:
        ponderado_tabla2 = salidajson.add_distancia_sinonimo_json(prop, ponderado_tabla2)
    print("Terminado Distancia Sinonimo")

    for key_dic_1 in diccionario1:
        propiedadDic1 = diccionario1[key_dic_1][0]
        if not coincidencia_exacta(propiedadDic1, array_para_salidas_dataset1):
            array_para_salidas_dataset1, array_para_salidas_dataset2 = calcular_distancias_significado(propiedadDic1, diccionario2, array_para_salidas_dataset1, array_para_salidas_dataset2)
        fila += 1

    for prop in array_para_salidas_dataset1:
        ponderado_tabla1 = salidajson.add_distancia_significado_json(prop, ponderado_tabla1)

    for prop in array_para_salidas_dataset2:
        ponderado_tabla2 = salidajson.add_distancia_significado_json(prop, ponderado_tabla2)

    salidajson.writeJSON(ponderado_tabla1, "salida_parcial_1", nombreRDF)
    salidajson.writeJSON(ponderado_tabla2, "salida_parcial_2", nombreRDF)

    print("Terminado Significado")

    try:
        array_para_salidas_dataset1  = lov.asignar_lov(array_para_salidas_dataset1, ratio)
        print("Terminado LOV 1")

        array_para_salidas_dataset2 = lov.asignar_lov(array_para_salidas_dataset2, ratio)
        print("Terminado LOV 2")

    except:
        array_para_salidas_dataset2 = lov.asignar_ontologias_lov_caido(array_para_salidas_dataset1, array_para_salidas_dataset2, ratio)
        errorLOV = True

    array_para_salidas_dataset1 = lov.preparar_datos_para_salida(diccionario1, array_para_salidas_dataset1)
    print("Terminado Salida1")

    array_para_salidas_dataset2 = lov.preparar_datos_para_salida(diccionario2, array_para_salidas_dataset2)
    print("Terminado Salida2")

    salidajson.writeJSON(array_para_salidas_dataset1, "rdf1", nombreRDF)
    salidajson.writeJSON(array_para_salidas_dataset2, "rdf2", nombreRDF)

    print("Terminado Escritura JSON")
    generateRDF.init_rdf(nombreRDF)
    return errorLOV


def coincidencia_exacta(propiedad, array_para_salidas):
    valor_maximo = False
    for i in array_para_salidas:
        if i.getName() == propiedad.getName() and i.getCoincidencia()[0] == 1:
            valor_maximo = True
    return valor_maximo


def add_propiedades_array_salida(diccionario1, diccionario2):
    array_para_salidas_dataset1 = []
    array_para_salidas_dataset2 = []
    for key_dic_1 in diccionario1:
        propiedadDic1 = diccionario1[key_dic_1][0]
        array_para_salidas_dataset1.append(propiedadDic1)

    for key_dic_2 in diccionario2:
        propiedadDic2 = diccionario2[key_dic_2][0]
        array_para_salidas_dataset2.append(propiedadDic2)
    return array_para_salidas_dataset1, array_para_salidas_dataset2


def start(diccionario1, diccionario2, ratio, nombreRDF):
    global fila
    global filaPonderado
    folder = str(nombreRDF)+'/'
    fila = 1
    filaPonderado = 0
    resultado = compareProps(diccionario1, diccionario2, ratio, nombreRDF)
    print("terminada comparacion")
    return resultado



def crear_carpetas_salida(nombreRDF):
    try:
        os.mkdir(str(sys.path[0])+'/'+str(nombreRDF))

    except:
        print ("Ya existe el directorio")


def main(rdf1, rdf2, ratio, nombre):
    # Def global variables
    global filaPonderado, diccionarioDistanciasPonderado
    global nombreRDF
    reload(sys)
    sys.setdefaultencoding('utf-8')

    nombreRDF = str(nombre)

    filaPonderado = 0
    diccionarioDistanciasPonderado = {}
    prefix.update_prefix()
    crear_carpetas_salida(nombreRDF)
    print("carpeta creada")
    try:
        diccionario1 = query.doQuery(rdf1, nombreRDF)
    except Exception as e:
        print('SalidaError', str(e))
        if "(application/xml, <class 'rdflib.parser.Parser'>)" in str(e):
            print("rdf1 no valido")
            return "RDF1_no_valido"

        else:
            return "errorRDF1"
        exit()
    try:
        diccionario2 = query.doQuery(rdf2, nombreRDF)
    except Exception as e:
        if str(e) == "No plugin registered for (application/xml, <class 'rdflib.parser.Parser'>)":
            return "RDF2_no_valido"
        return "errorRDF2"
        exit()
    if start(diccionario1, diccionario2, ratio, nombreRDF) == True :
        return "errorLOV"


if __name__ == "__main__":
    sys.exit(main())
