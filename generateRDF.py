import sys
from rdflib import Namespace, URIRef, Graph

from rdflib import URIRef, BNode, Literal, XSD
import prefix
import rdflib
import json

def mis_bindeos(bindeos, numBindeo, ontologia_nombre):

    if ontologia_nombre not in bindeos:
        bindeos[ontologia_nombre] = 'ns' + str(numBindeo)
        numBindeo += 1
    return bindeos, numBindeo, bindeos[ontologia_nombre]


def generateRDF(nombre, diccionario_bindeos, valor_bindeos, nombreRDF, ambos):
    val = 0
    if ambos != False:
        fichero_json = json.loads(open(str(sys.path[0]) + '/' + nombreRDF + '/' + nombre + '.json').read())
        g = Graph()
        for i in fichero_json:
            if i["uri_esBlank"] == "URIRef":
                uri = rdflib.URIRef(str(i["uri"]))

            elif i["uri_esBlank"] == "Bnode":
                uri = rdflib.BNode(str(i["uri"]))
            else:
                uri = Literal(i["value"])

            ontology = rdflib.URIRef(i["ontology"])

            if i["value_esBlank"] == "URIRef":
                value = rdflib.URIRef(str(i["value"]).replace(" ", ""))

            elif i["value_esBlank"] == "Bnode":
                value = rdflib.BNode(str(i["value"]))

            else:
                if i["dataype"] == "decimal":
                    value = Literal(i["value"], datatype=XSD.Decimal)
                    # value = Literal(str(), datatype=XSD.decimal)
                elif i["dataype"] == "integer":
                    value = Literal(str(i["value"]), datatype=XSD.long)
                elif i["dataype"] == "date":
                    value = Literal(str(i["value"]), datatype=XSD.date)
                else:
                    value = Literal(i["value"])
            if i["prefix"] == None:
                diccionario_bindeos, valor_bindeos, prefijo = mis_bindeos(diccionario_bindeos, valor_bindeos, prefix.get_for_prefix(ontology))
                g.bind(str(prefijo), str(prefix.get_for_prefix(ontology)))
            else:
                g.bind(str(i["prefix"]), str(prefix.get_for_prefix(ontology)))
                # array_bindeados.append(ontology)
            g.add((uri, ontology, value))
        f = open(str(sys.path[0])+'/'+str(nombreRDF)+'/'+str(nombre)+'.ttl', 'w')
        # print(i["uri"], i["ontology"], i["value"])
        f.write(g.serialize(format="n3").replace("Decimal", "decimal"))  # python will convert \n to os.linesep
        f.close()
        return diccionario_bindeos, valor_bindeos
    else:
        g = Graph()
        val = 0
        fichero_json1 = json.loads(open(str(sys.path[0]) + '/' + nombreRDF + '/' + 'rdf1.json').read())
        fichero_json2 = json.loads(open(str(sys.path[0]) + '/' + nombreRDF + '/' + 'rdf2.json').read())
        for i in fichero_json1:
            if i["uri_esBlank"] == "URIRef":
                uri = rdflib.URIRef(str(i["uri"]))

            elif i["uri_esBlank"] == "Bnode":
                uri = rdflib.BNode(str(i["uri"]))
            else:
                uri = Literal(i["value"])

            ontology = rdflib.URIRef(i["ontology"])

            if i["value_esBlank"] == "URIRef":
                value = rdflib.URIRef(str(i["value"]).replace(" ", ""))

            elif i["value_esBlank"] == "Bnode":
                value = rdflib.BNode(str(i["value"]))

            else:
                if i["dataype"] == "decimal":
                    value = Literal(i["value"], datatype=XSD.Decimal)
                    # value = Literal(str(), datatype=XSD.decimal)
                elif i["dataype"] == "integer":
                    value = Literal(str(i["value"]), datatype=XSD.long)
                elif i["dataype"] == "date":
                    value = Literal(str(i["value"]), datatype=XSD.date)
                else:
                    value = Literal(i["value"])
            if i["prefix"] == None:
                diccionario_bindeos, valor_bindeos, prefijo = mis_bindeos(diccionario_bindeos, valor_bindeos, prefix.get_for_prefix(ontology))
                g.bind(str(prefijo), str(prefix.get_for_prefix(ontology)))
            else:
                g.bind(str(i["prefix"]), str(prefix.get_for_prefix(ontology)))
                # array_bindeados.append(ontology)
            g.add((uri, ontology, value))

        for i in fichero_json2:
            if i["uri_esBlank"] == "URIRef":
                uri = rdflib.URIRef(str(i["uri"]))

            elif i["uri_esBlank"] == "Bnode":
                uri = rdflib.BNode(str(i["uri"]))
            else:
                uri = Literal(i["value"])

            ontology = rdflib.URIRef(i["ontology"])

            if i["value_esBlank"] == "URIRef":
                value = rdflib.URIRef(str(i["value"]).replace(" ", ""))

            elif i["value_esBlank"] == "Bnode":
                value = rdflib.BNode(str(i["value"]))

            else:
                if i["dataype"] == "decimal":
                    value = Literal(i["value"], datatype=XSD.Decimal)
                    # value = Literal(str(), datatype=XSD.decimal)
                elif i["dataype"] == "integer":
                    value = Literal(str(i["value"]), datatype=XSD.long)
                elif i["dataype"] == "date":
                    value = Literal(str(i["value"]), datatype=XSD.date)
                else:
                    value = Literal(i["value"])
            if i["prefix"] == None:
                diccionario_bindeos, valor_bindeos, prefijo = mis_bindeos(diccionario_bindeos, valor_bindeos, prefix.get_for_prefix(ontology))
                g.bind(str(prefijo), str(prefix.get_for_prefix(ontology)))
            else:
                g.bind(str(i["prefix"]), str(prefix.get_for_prefix(ontology)))
                # array_bindeados.append(ontology)
            g.add((uri, ontology, value))
        f = open(str(sys.path[0]) + '/' + str(nombreRDF) + '/' + str("esquema_conjunto") + '.ttl', 'w')
        # print(i["uri"], i["ontology"], i["value"])
        f.write(g.serialize(format="n3").replace("Decimal", "decimal"))  # python will convert \n to os.linesep
        f.close()
        return diccionario_bindeos, valor_bindeos


def init_rdf(nombreRDF):
    reload(sys)
    print "Iniciando RDF"
    sys.setdefaultencoding('utf-8')
    diccionario_bindeos = {}
    valor_bindeos = 0
    diccionario_bindeos, valor_bindeos = generateRDF("rdf1", diccionario_bindeos, valor_bindeos, nombreRDF, True)
    diccionario_bindeos, valor_bindeos = generateRDF("rdf2", diccionario_bindeos, valor_bindeos, nombreRDF, True)
    generateRDF("rdf1,rdf2", diccionario_bindeos, valor_bindeos, nombreRDF, False)
    print "Fin RDF"


def main():
    reload(sys)
    diccionario_bindeos = {}
    valor_bindeos = 0
    sys.setdefaultencoding('utf-8')
    generateRDF("rdf2", diccionario_bindeos, valor_bindeos)



if __name__ == "__main__":
    sys.exit(main())
