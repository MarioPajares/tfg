#!/usr/local/bin/python
# -*- encoding: utf-8 -*-
from nltk.stem import WordNetLemmatizer
import sys


def doLemma(word):
    reload(sys)
    sys.setdefaultencoding('utf-8')
    return WordNetLemmatizer().lemmatize(word)

def main():
    word = "pages"
    print doLemma(word)


if __name__ == "__main__":
    sys.exit(main())
