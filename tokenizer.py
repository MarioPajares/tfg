from math import log
import nltk
from nltk import word_tokenize
import pattern.en as pattern

# Build a cost dictionary, assuming Zipf's law and cost = -math.log(probability).
words = open("/usr/share/dict/american-english").read().split()
wordcost = dict((k, log((i + 1) * log(len(words)))) for i, k in enumerate(words))
maxword = max(len(x) for x in words)


def infer_spaces(s):


    def best_match(i):
        candidates = enumerate(reversed(cost[max(0, i - maxword):i]))
        return min((c + wordcost.get(s[i - k - 1:i], 9e999), k + 1) for k, c in candidates)
    cost = [0]
    for i in range(1, len(s) + 1):
        c, k = best_match(i)
        cost.append(c)
    out = []
    i = len(s)
    while i > 0:
        c, k = best_match(i)
        assert c == cost[i]
        out.append(s[i - k:i])
        i -= k
    return " ".join(reversed(out))


def doTokenizer(palabra):
    salida = []
    lines = infer_spaces(palabra)
    for word in lines.split(" "):
        wordaux = pattern.parse(word, relations=True, lemmata=True)
        word = wordaux.split("/")[-1]
        text = word_tokenize((word))
        if len(word) > 1:
            if nltk.pos_tag(text)[0][1] == 'NN' or nltk.pos_tag(text)[0][1] == 'JJ':
                salida.append(nltk.pos_tag(text)[0][0])
    if len(salida) == 0:
        salida.append(palabra)
    return salida


