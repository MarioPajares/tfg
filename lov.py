import requests
import sys
import salidajson


class Ontology:

    def __init__(self, prefixedName, ratio, uri):
        self.prefixedName = prefixedName
        self.ratio = ratio
        self.uri = uri

    def getName(self):
        return self.prefixedName

    def getRatio(self):
        return self.ratio

    def getUri(self):
        return self.uri


def asignar_lov(array_para_salidas, ratio):
    array_auxiliar = []

    for propiedad_array in array_para_salidas:
        if propiedad_array.getCoincidencia() != []:
            propiedad2 = propiedad_array.getCoincidencia()
            if propiedad2[0] > ratio:
                prefixLove = getLov(propiedad_array.getName(), propiedad2[1].getName())
                if prefixLove != None:
                    prefixLove = prefixLove.getUri()
                    propiedad_array.setOntology(prefixLove)
                else:
                    prefixLove = propiedad_array.getProp()
                    propiedad_array.setOntology(prefixLove)
        else:
            prefixLove = getLov(propiedad_array.getName(), propiedad_array.getName())
            if prefixLove != None:
                prefixLove = prefixLove.getUri()
                propiedad_array.setOntology(prefixLove)
            else:
                prefixLove = propiedad_array.getProp()
                propiedad_array.setOntology(prefixLove)
        array_auxiliar.append(propiedad_array)
    return array_auxiliar


def preparar_datos_para_salida(diccionario1, array_para_salidas):
    array_auxiliar = []
    for key in diccionario1:
        propiedades = diccionario1[key]
        if "fax" in propiedades[0].getName():
            pass
        for propiedad in propiedades:
            for prop_array in array_para_salidas:
                if propiedad.getName() == prop_array.getName():
                    propiedad.setOntology(prop_array.getOntology())
                    array_auxiliar = salidajson.addJSON(propiedad, array_auxiliar)
                    break
    return array_auxiliar


def asignar_ontologias_lov_caido(array1, array2, ratio):
    array_auxiliar = []
    for prop2 in array2:
        if prop2.getCoincidencia() != []:
            if prop2.getCoincidencia()[0] > ratio:
                for prop1 in array1:
                    if prop1.getName() == prop2.getCoincidencia()[1].getName():
                        prop2.setOntology(prop1.getOntology())
                        break
        array_auxiliar.append(prop2)
    return array_auxiliar


def doQuery(numPag, numLODdatasets, arrayResultados, word):
    global numLOV
    arraySalida = []
    r = requests.get(
        'http://lov.okfn.org/dataset/lov/api/v2/term/search?q=' + str(word) + '&type=property&page=' + str(numPag))
    resultados = r.json()["results"]
    for i in resultados:
        if i["metrics.reusedByDatasets"][0] > 0:
            numLODdatasets += float(i["metrics.reusedByDatasets"][0])
            arrayResultados.append(i)
    numPag = numPag + 1
    if numPag < 3:
        arraySalida = doQuery(int(numPag), numLODdatasets, arrayResultados, word)
    else:
        for i in arrayResultados:
            ratio = float(i["metrics.occurrencesInDatasets"][0]) * i["metrics.reusedByDatasets"][0] / numLODdatasets
            ontology = Ontology(i["prefixedName"][0], ratio, i["uri"][0])
            arraySalida.append(ontology)
    return sorted(arraySalida, key=lambda Ontology: Ontology.getRatio(), reverse=True)


def getLov(word1, word2):
    global numLOV
    numLOV = 3
    numLODdatasets = 0
    iguales = None
    if word1 == word2:
        arrayResultados = []
        salida = doQuery(1, numLODdatasets, arrayResultados, word1)
        if len(salida) > 0:
            arrayResultados = salida
            iguales = Ontology(arrayResultados[0].getName(), arrayResultados[0].getRatio(), arrayResultados[0].getUri())
        return iguales
    else:
        arrayResultados1 = []
        arrayResultados2 = []
        salida1 = doQuery(1, numLODdatasets, arrayResultados1, word1)
        arrayResultados1 = salida1
        salida2 = doQuery(1, numLODdatasets, arrayResultados2, word2)
        arrayResultados2 = salida2
        if len(arrayResultados1) > 0 and len(arrayResultados2) > 0:
            for i in arrayResultados1:
                for p in arrayResultados2:
                    if i.prefixedName == p.prefixedName:
                        iguales = i
                        break
            if iguales is not None:
                return iguales
            else:
                if arrayResultados1[0].ratio > arrayResultados2[0].ratio:
                    iguales = arrayResultados1[0]
                else:
                    iguales = arrayResultados2[0]
                return iguales
        elif len(arrayResultados1) > 0:
            iguales = arrayResultados1[0]
            return iguales

        elif len(arrayResultados2) > 0:
            iguales = arrayResultados2[0]
            return iguales


def main():
    reload(sys)
    sys.setdefaultencoding('utf-8')
    ontolo = Ontology
    word1 = 'phone'

    ontolo = getLov(word1, word1)

    if ontolo is not None:
        print ontolo.getUri()
    else:
        print ontolo


if __name__ == "__main__":
    sys.exit(main())
