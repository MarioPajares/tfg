import json
import sys
from flask import Flask, send_from_directory
from forms import Form
from flask import render_template, flash, redirect, request
from config import Config
import sqlite


import main
application = Flask(__name__)
# app.jinja_env.add_extension('jinja2.ext.do')
application.config.from_object(Config)

@application.route('/', methods=['GET', 'POST'])
def start_api():
    form = Form()
    data = ["", "", ""]
    ip = request.remote_addr
    print("Cargdo desde", str(ip))
    if form.validate_on_submit():
        if form.validate_on_submit():
            flash('Login requested for user {}, remember_me={}'.format(
                form.rdf1.data, form.rdf2.data))
            return redirect('/index')
    return render_template('index.html', form=form, data=json.dumps(data))


@application.route('/home')
def resultados():
    global nombreRDF
    ip = request.remote_addr
    error = False
    data = ["", "", "", "", ""]
    rdf1 = request.args.get('rdf1')
    rdf2 = request.args.get('rdf2')
    ratio = request.args.get('ratio')
    try:
        num = float(ratio)
    except:
        print ("Error ratio")
        data[0] = "HayError"
        error = True
    if error != True:
        if 0 < float(ratio) <= 1.0:
            resultado = main.main(str(rdf1), str(rdf2), float(ratio), str(ip))
            if resultado == "errorRDF1":
                data[1] = "HayError"
                error = True
            elif resultado == "errorRDF2":
                data[2] = "HayError"
                error = True
            elif resultado == "RDF1_no_valido":
                data[3] = "RDF1_bad"
                data[1] = "HayError"
                error = True
            elif resultado == "RDF2_no_valido":
                data[3] = "RDF2_bad"
                data[2] = "HayError"
                error = True
            else:
                if resultado == "errorLOV":
                    data = ["errorLOV"]
                errores1 = json.loads(open(str(sys.path[0]) + '/' + str(ip) + '/' 'salida_parcial_1.json').read())
                errores2 = json.loads(open(str(sys.path[0]) + '/' + str(ip) + '/' 'salida_parcial_2.json').read())
                print ("Finalizada ejecucion")
                return render_template('results.html', data=json.dumps(data), errores1=json.dumps(errores1), errores2=json.dumps(errores2), ratio=ratio)

        else:
            data[0] = "HayError"
            error = True
    if error == True:
        form = Form()
        if form.validate_on_submit():
            if form.validate_on_submit():
                flash('Login requested for user {}, remember_me={}'.format(
                    form.rdf1.data, form.rdf2.data))
                return redirect('/index')
        return render_template('index.html', form=form,  data=json.dumps(data))


@application.route("/descarga1", methods=['GET'])
def descarga1():
    ip = request.remote_addr
    print(str(sys.path[0]) + "/" + str(ip))
    return send_from_directory(str(sys.path[0]) + "/" + str(ip), "rdf1.ttl", as_attachment=True)


@application.route("/descarga2", methods=['GET'])
def descarga2():
    ip = request.remote_addr
    print(str(sys.path[0]) + "/" + str(ip))
    return send_from_directory(str(sys.path[0]) + "/" + str(ip), "rdf2.ttl", as_attachment=True)


@application.route("/conjunta", methods=['GET'])
def descarga1_distancias():
    ip = request.remote_addr
    print(str(sys.path[0]) + "/" + str(ip))
    return send_from_directory(str(sys.path[0]) + "/" + str(ip), "esquema_conjunto.ttl", as_attachment=True)


if __name__ == '__main__':
    application.run()

