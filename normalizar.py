#!/usr/local/bin/python
# -*- encoding: utf-8 -*-
import sys
import re


def normalizar(word):
    reload(sys)
    sys.setdefaultencoding('utf-8')
    resultado = word.lower()
    resultado = resultado.replace('á', 'a')
    resultado = resultado.replace('ά', 'a')
    resultado = resultado.replace('é', 'e')
    resultado = resultado.replace('í', 'i')
    resultado = resultado.replace('ó', 'o')
    resultado = resultado.replace('ú', 'u')
    resultado = resultado.replace('à', 'a')
    resultado = resultado.replace('è', 'e')
    resultado = resultado.replace('ì', 'i')
    resultado = resultado.replace('ò', 'o')
    resultado = resultado.replace('ù', 'u')
    resultado = resultado.replace('â', 'a')
    resultado = resultado.replace('ê', 'e')
    resultado = resultado.replace('î', 'i')
    resultado = resultado.replace('ô', 'o')
    resultado = resultado.replace('û', 'u')
    resultado = resultado.replace('ä', 'a')
    resultado = resultado.replace('ë', 'e')
    resultado = resultado.replace('ì', 'i')
    resultado = resultado.replace('ò', 'o')
    resultado = resultado.replace('ù', 'u')
    resultado = resultado.replace('ș', 's')
    resultado = resultado.replace('Ç', 'c')
    resultado = resultado.replace('ĉ', 'c')
    resultado = resultado.replace('č', 'c')
    resultado = resultado.replace('ñ', 'n')
    resultado = resultado.replace('Ù', 'u')
    resultado = resultado.replace('Û', 'u')
    resultado = resultado.replace('Ü', 'u')
    resultado = resultado.replace('Þ', 'b')
    resultado = resultado.replace('ß', 'ss')
    resultado = resultado.replace('Ð', 'd')
    resultado = resultado.replace('Ĵ', 'j')
    resultado = resultado.replace('ĵ', 'j')
    resultado = resultado.replace('ά', 'a')
    resultado = resultado.replace('Ÿ', 'y')
    resultado = resultado.replace('ƒ', 'f')
    resultado = resultado.replace('_', '')
    resultado = resultado.replace('-', '')
    resultado = resultado.replace(R'\s', '')
    resultado = resultado.replace('0', '')
    resultado = resultado.replace('1', '')
    resultado = resultado.replace('2', '')
    resultado = resultado.replace('3', '')
    resultado = resultado.replace('4', '')
    resultado = resultado.replace('5', '')
    resultado = resultado.replace('6', '')
    resultado = resultado.replace('7', '')
    resultado = resultado.replace('8', '')
    resultado = resultado.replace('9', '')

    regex1 = re.compile(r"\W*", re.IGNORECASE)
    regex3 = re.compile(r"\s*", re.IGNORECASE)
    resultado = regex1.sub('', resultado)
    resultado = regex3.sub('', resultado)

    return resultado


def main():
    word = "localización"
    print (normalizar(word))


if __name__ == "__main__":
    sys.exit(main())
