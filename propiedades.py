import sys
from tokenizer import doTokenizer
import Lemma
import normalizar


class Propiedad:

    def __init__(self, prop, value, ontology, url, uri_esBlank, value_esBlank, datetime):
        self.prop = prop
        self.value = value
        self.propName = Lemma.doLemma(getClassName(prop))
        self.ontology = ontology
        self.url = url
        self.coincidencia = []
        self.uri_esBlank = uri_esBlank
        self.value_esBlank = value_esBlank
        self.datetime = datetime

    def get_value_esBlank(self):
        return self.value_esBlank

    def get_uri_esBlank(self):
        return self.uri_esBlank

    def getProp(self):
        return self.prop

    def getValue(self):
        return self.value

    def getName(self):
        return self.propName

    def getUri(self):
        return self.url

    def get_datatype(self):
        return self.datetime

    def getOntology(self):
        return self.ontology

    def getAll(self):
        return self.prop, " ", self.value, self.ontology

    def setOntology(self, ontology):
        self.ontology = ontology

    def setCoincidencia(self, array):
        self.coincidencia = array

    def getCoincidencia(self):
        return self.coincidencia

    def get_value_coincidencia(self):
        return self.coincidencia[0]


def getClassName(word):

    if (len(str(word).split("#"))) > 1:
        return normalizar.normalizar(str(word).split("#")[1])
    else:
        return normalizar.normalizar(str(word).split("/")[-1])

def main():
    propiedad1 = Propiedad("http://dbpedia.org/property/criteria", "sdsaddas", "", "")
    print propiedad1.getAll()
    propiedad1.setOntology("ontologia")


if __name__ == "__main__":
    sys.exit(main())