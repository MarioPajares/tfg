from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField



class Form(FlaskForm):
    rdf1 = StringField('RDF1')
    rdf2 = StringField('RDF2')
    ratio = StringField('RATIO [0-1]')
    nombreRDF = StringField('Nombre Salida')
    submit = SubmitField('Sign In')
    valorDefecto = "valorDefecto"

