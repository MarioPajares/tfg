import sys

from rdflib import Graph
from rdflib import Literal
import propiedades


def insert_data(qres):
    diccionarioProps = {}
    a = qres.result
    for i in qres.result:
        prueba = Literal(i[2]).datatype
        if "decimal" in str(prueba):
            date_time = "decimal"
        elif "dateTime" in str(prueba):
            date_time = "date"
        elif "integer" in str(prueba) or "int" in str(prueba):
            date_time = "integer"
        else:
            date_time = "None"
        value_esBlank = check_is_Bnode_orURIRef(str(type(i[2])))
        uri_esBlank = check_is_Bnode_orURIRef(str(type(i[0])))
        if i[1] not in diccionarioProps:
            diccionarioProps[i[1]] = []
        propiedad = propiedades.Propiedad(i[1], i[2], i[1], i[0], uri_esBlank, value_esBlank, date_time)
        diccionarioProps[i[1]].append(propiedad)

    return diccionarioProps


def check_is_Bnode_orURIRef(word):
    if "rdflib.term.BNode" in str(word):
        return "Bnode"
    elif "rdflib.term.URIRef" in str(word):
        return "URIRef"
    else:
        return False


def doQuery(rdf, nombreRDF):

    f = open(str(sys.path[0])+'/'+str(nombreRDF)+"/rdfs.txt", "a")
    f.write(rdf)
    f.write('\n')
    # f.write(rdf2)
    f.close()

    nombre_rdf1 = rdf.split('//')[1].split('/')[0]

    g = Graph()
    g2 = Graph()

    if "ttl" in rdf:
        g.parse(str(rdf), format="ttl")
    elif "n3" in rdf:
        g.parse(str(rdf), format="n3")
    else:
        g.parse(str(rdf))

    # CONSULTA SPARQL
    qres = g.query("""select DISTINCT ?URI ?prop ?a  where{?URI ?prop ?a.} """)

    diccionarioProps1 = insert_data(qres)

    print "Escritas propiedades"
    return diccionarioProps1



def main():
    rdf1 = 'https://datos.madrid.es/egob/catalogo/202311-0-colegios-publicos.rdf'
    rdf2 = 'https://datos.madrid.es/egob/catalogo/202311-0-colegios-publicos.rdf'
    doQuery(rdf1, rdf2, "Ejecucionpruebas")

if __name__ == "__main__":
    sys.exit(main())
