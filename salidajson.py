import sys
import json
import query
import prefix


def addJSON(propiedad, a ):
    diccionario = {"uri": propiedad.getUri(), "prop": propiedad.getProp(), "value": propiedad.getValue(),
                   "ontology": propiedad.getOntology(), "value_esBlank": propiedad.get_value_esBlank(), "uri_esBlank": propiedad.get_uri_esBlank(), "prefix": prefix.check_prefix(propiedad.getOntology()), "dataype": propiedad.get_datatype()}
    a.append(diccionario)
    return a


def add_distancia_semantica_json(propiedad, a):
    if propiedad.getCoincidencia():
        diccionario = {"prop": propiedad.getName(), "distancia_semantica": {"propiedad": propiedad.getCoincidencia()[1].getName(), "valor": str(propiedad.getCoincidencia()[0])[0:5]}, "distancia_sinonimos": {}, "distancia_significado": {}, "total": {}, "ontologia":{}}
    else:
        diccionario = {"prop": propiedad.getName(), "distancia_semantica": {"propiedad": "-", "valor": "-" }, "distancia_sinonimos": {}, "distancia_significado": {}, "total": {}}

    a.append(diccionario)
    return a


def add_distancia_sinonimo_json(propiedad, a):
    for diccionario in a:
        if diccionario["prop"] == propiedad.getName():
            if propiedad.getCoincidencia():
                diccionario["distancia_sinonimos"] = {'propiedad': propiedad.getCoincidencia()[1].getName(), "valor": str(propiedad.getCoincidencia()[0])[0:5]}
            else:
                diccionario["distancia_sinonimos"] = {'propiedad': "-", "valor": "-"}
    return a


def add_distancia_significado_json(propiedad, a):
    for diccionario in a:
        if diccionario["prop"] == propiedad.getName():
            if propiedad.getCoincidencia():
                diccionario["distancia_significado"] = {'propiedad': propiedad.getCoincidencia()[1].getName(), "valor": str(propiedad.getCoincidencia()[0])[0:5]}
            else:
                diccionario["distancia_significado"] = {'propiedad': "-", "valor": "-"}
            diccionario["ontologia"] = str(propiedad.getOntology())
    return a


def writeJSON(a, name, nombreRDF):
    salida = json.dumps(a)
    f = open(str(sys.path[0])+'/'+str(nombreRDF)+'/'+name+'.json', 'w')
    f.write(salida)
    f.close()


def main():
    a = []
    rdf1 = 'https://data.cityofchicago.org/api/views/x8fc-8rcq/rows.rdf?accessType=DOWNLOAD'
    salidas = query.doQuery(rdf1)
    propiedades1 = salidas[0]

    for key in propiedades1:
        a = addJSON(propiedades1[key][0], a)
    writeJSON(a)


if __name__ == "__main__":
    sys.exit(main())