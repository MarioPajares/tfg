import sqlite3
import sys
import abbreviationsAPI
import normalizar


def insert_data(word_abbreviation, long_word):
    con = sqlite3.connect(str(sys.path[0]) + '/abbreviations')
    cursor = con.cursor()
    print("Insertado", str(word_abbreviation), "->", str(long_word))
    query = '''INSERT INTO abreviaciones (ID, longWord) VALUES ("''' +str(word_abbreviation)+'''","'''+ str(long_word)+'''")'''
    cursor.execute(query)
    con.commit()


def create_table():
    con = sqlite3.connect(str(sys.path[0]) + '/abbreviations')
    cursor = con.cursor()
    cursor.execute('''CREATE TABLE IF NOT EXISTS abreviaciones(ID CHAR(30) PRIMARY KEY NOT NULL, longWord CHAR(50) NOT NULL )''')
    print("Tabla creada")


def find_abbreviation(id):
    if id == "tel":
        pass
    con = sqlite3.connect(str(sys.path[0]) + '/abbreviations')
    cursor = con.cursor()
    cursor.execute('''SELECT ID, longWord FROM abreviaciones WHERE ID="'''+str(id)+'''"''')
    word = ""
    for i in cursor:
        word = i[1]
    if len(word) == 0:
        long_Word = abbreviationsAPI.get_abbreviations(id)
        insert_data(id, long_Word)
        return normalizar.normalizar(long_Word)
    else:
        return normalizar.normalizar(word)


def main():
    find_abbreviation("tel")


if __name__ == "__main__":
    sys.exit(main())
