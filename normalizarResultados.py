#!/usr/local/bin/python
# -*- encoding: utf-8 -*-
import sys


def normalizarSalida(val, numCaracteres):
    if numCaracteres > 0:
        if numCaracteres > val:
            resultado = float(float(val) / float(numCaracteres))
            return float(1 - resultado)

        else:
            return float(0)
    else:
        return float(0)


def normalizarTamPalabras(word1, word2):
    while len(word2) < len(word1):
        word2 = ' '+str(word2)
    while len(word1) < len(word2):
        word1 = ' ' + str(word1)
    return word1, word2


def main():
    result = normalizarSalida(6, 20)
    print (result)


if __name__ == "__main__":
    sys.exit(main())