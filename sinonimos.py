import sys
from nltk.corpus import wordnet
from difflib import SequenceMatcher
import tokenizer

array = []

def similar(a, b):
    return SequenceMatcher(None, a, b).ratio()


def getSinonimos(word):
    sinonimos = []
    syns = wordnet.synsets(word)
    for i in syns:
        i = str(i)
        i = i[i.find("(")+2:i.find(".")]
        if i not in sinonimos:
            sinonimos.append(i)
    return sinonimos


def main():
    tokenizado = tokenizer.doTokenizer("location")
    print tokenizado[0]
    for i in getSinonimos(tokenizado[0]):
        print(i)


if __name__ == "__main__":
    sys.exit(main())
