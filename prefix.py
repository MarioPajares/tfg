import json
import sys
import requests


def update_prefix():
    r = requests.get("http://prefix.cc/popular/all.file.json")
    if r.status_code is 200:
        salida = json.dumps(r.json())
        # print(salida[0])
        f = open(str(sys.path[0]) + '/prefix/prefijos.json', 'w')
        f.write(salida)
        f.close()
        print("descargado")


def check_prefix(word):
    prefijos_json = json.loads(open(str(sys.path[0]) + '/prefix/prefijos.json').read())
    for i in prefijos_json:
        # print i, prefijos_json[i]
         if get_for_prefix(word) == prefijos_json[i]:
            return i
            break


def get_for_prefix(word):
    if (len(str(word).split("#"))) > 1:
        return str(word).split("#")[0]+"#"
    else:
        return str(word).replace(str(word).split("/")[-1], "")


def main():
    w2 = "http://www.aktors.org/ontology/portal#addresses-generic-area-of-interest"
    update_prefix()
    print(check_prefix(get_for_prefix(w2)))

if __name__ == "__main__":
    sys.exit(main())
